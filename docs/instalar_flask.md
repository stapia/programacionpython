
# Instalar flask

## Instalar con pip3

Directamente (cuidado con las mayúsculas):

```
sudo su -
pip3 install Flask Flask-RESTful
```

Al hacerlo con sudo hace que se instale para todos los usuarios. 

## Ejecutar una aplicación mínima:

Buscar el código en (Minimal API):

https://flask-restful.readthedocs.io/en/latest/quickstart.html

Copiar el texto en un archivo, guardarlo como "api.py" y ejecutarlo con:

```
python3 api.py
```

Esto solo se puede probar localmente. Para hacer que se pueda navegar desde fuera,
hay que hacer:

```
export FLASK_APP=api.py
flask run --host=0.0.0.0
```

# Instalar servidor WSGI

Al arrancar se dice: "Use a production WSGI server instead",

Un servidor WSGI es alguno de los servidores habituales: apache2, ngnix... que
tiene algún modulo o extensión especial para interactuar con código en python 
(igual que lo tienen, por ejemplo, para interactuar con PHP).

Vamos a configurar apache2 con esto. Se puede seguir:

http://terokarvinen.com/2017/write-python-3-web-apps-with-apache2-mod_wsgi-install-ubuntu-16-04-xenial-every-tiny-part-tested-separately

1) Instalar el modulo para apache2:

```
sudo apt-get install libapache2-mod-wsgi-py3
```

Para abreviar el proceso de prueba del servidor se puede añadir en 
el sitio por defecto:

```
WSGIScriptAlias /app /var/www/html/.hidden/application.py
```

Y entonces al poner el nombre del servidor y /app ya se ejecuta el script.

Para probar el de FlaskRESTful se puede mover al mismo sitio y añadir:

WSGIScriptAlias /api /var/www/html/.hidden/api.py

## Configurar como site

Para configurar como un "sitio" se puede poner en site-available un archivo con:

```
<VirtualHost *:5001>
    ServerName py.aplicacion.upm.es
    ErrorLog ${APACHE_LOG_DIR}/error-wsgi.log
    CustomLog ${APACHE_LOG_DIR}/access-wsgi.log combined

    WSGIScriptAlias / /ruta/app1/application.py

    <Directory /ruta/app1>
        Require all granted
    </Directory>
</VirtualHost>
```

Si se configura con un puerto distinto del 80 hay que añadirlo en port.conf

## Gestión de Anaconda 

1) Para (des)activar anaconda al inicio 

conda config --set auto_activate_base false

1) Para activar o desactivar conda, es decir, un entorno:

conda activate base
conda deactivate
