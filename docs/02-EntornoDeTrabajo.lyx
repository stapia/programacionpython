#LyX 2.3 created this file. For more info see http://www.lyx.org/
\lyxformat 544
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass beamer
\begin_preamble
%\usetheme{Warsaw}
% or ...
\usetheme{Boadilla}

\usecolortheme{whale}
%\usecolortheme{albatross}

\setbeamercovered{transparent}
% or whatever (possibly just delete it)
\end_preamble
\use_default_options false
\master 00-Todas.lyx
\maintain_unincluded_children false
\language spanish
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "lmodern" "default"
\font_sans "lmss" "default"
\font_typewriter "lmtt" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\use_microtype false
\use_dash_ligatures false
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\spacing single
\use_hyperref false
\papersize default
\use_geometry true
\use_package amsmath 2
\use_package amssymb 2
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 0
\use_minted 0
\index Índice
\shortcut idx
\color #008000
\end_index
\secnumdepth 2
\tocdepth 2
\paragraph_separation indent
\paragraph_indentation default
\is_math_indent 0
\math_numbering_side default
\quotes_style english
\dynamic_quotes 0
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Section
Variables y Expresiones
\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Ejemplos sencillos de python
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout FrameSubtitle
Uso como calculadora
\end_layout

\end_deeper
\begin_layout Frame
Vamos a usar python como una pequeña calculadora con memoria.
\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame
Queremos fijarnos especialmente en las operaciones posibles y en los tipos
 de datos.
\end_layout

\begin_layout Frame

\end_layout

\begin_layout Frame
Intenta hacer las siguientes operaciones:
\end_layout

\begin_deeper
\begin_layout ExampleBlock
\begin_inset Argument 2
status open

\begin_layout Plain Layout
Operaciones aritméticas
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout Itemize
Suma, resta, multiplica y divide números enteros
\end_layout

\begin_layout Itemize
Lo mismo con números en coma flotante
\end_layout

\begin_layout Itemize
Prueba a escribir números en notación científica por ejemplo: 1e-6
\end_layout

\begin_layout Itemize
Si pones una j detrás de un número tendrás un número complejo
\end_layout

\begin_layout Itemize
Busca cómo se pone una potencia (en inglés 
\emph on
\lang english
raise to power of
\emph default
\lang spanish
)
\end_layout

\begin_layout Itemize
Busca cómo se hace el cociente entero.
 En inglés 
\emph on
\lang english
integer quotient
\emph default
\lang spanish
.
\end_layout

\begin_layout Itemize
Y el módulo (resto de la división entera, en inglés 
\emph on
\lang english
integer remainder)
\end_layout

\end_deeper
\begin_layout ExampleBlock

\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Otras operaciones
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout ExampleBlock
\begin_inset Argument 2
status open

\begin_layout Plain Layout
De relación
\end_layout

\end_inset


\end_layout

\begin_layout ExampleBlock
Comparaciones, dan como resultado True ó False:
\end_layout

\begin_layout ExampleBlock

\family typewriter
==
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset

!=
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset

>=
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset

<=
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset

>
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset

< 
\end_layout

\begin_layout ExampleBlock
respectivamente igual, distinto, mayor o igual...
\end_layout

\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout ExampleBlock
\begin_inset Argument 2
status open

\begin_layout Plain Layout
Lógicos
\end_layout

\end_inset


\end_layout

\begin_layout ExampleBlock
Operadores lógicos o booleanos, resultado True ó False:
\end_layout

\begin_layout ExampleBlock

\family typewriter
and
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset

or
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset

not
\end_layout

\begin_layout ExampleBlock
(equivalen a su significado habitual en lenguaje natural...)
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Otras operaciones (continuación)
\end_layout

\end_inset


\end_layout

\begin_deeper
\begin_layout ExampleBlock
\begin_inset Argument 2
status open

\begin_layout Plain Layout
De bit o bitwise
\end_layout

\end_inset


\end_layout

\begin_layout ExampleBlock

\end_layout

\begin_layout ExampleBlock

\family typewriter
&
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset

|
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset

^
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset

~
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\family default
respectivamente and, or, xor, not bitwise
\end_layout

\begin_layout ExampleBlock

\family typewriter
<<
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset

>>
\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\begin_inset space ~
\end_inset


\family default
operadores de desplazamiento
\end_layout

\begin_layout ExampleBlock

\end_layout

\begin_layout ExampleBlock
Son los mismos que en C...
 
\end_layout

\begin_layout ExampleBlock
Las operaciones de bit se suelen utilizar en aplicaciones de bajo nivel
\end_layout

\begin_layout ExampleBlock
Es raro tener que usarlo lo normal es usar las anteriores (las lógicas)
\end_layout

\begin_layout ExampleBlock

\end_layout

\begin_layout ExampleBlock

\end_layout

\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout ExampleBlock
\begin_inset Argument 2
status open

\begin_layout Plain Layout
Concatenación
\end_layout

\end_inset


\end_layout

\begin_layout ExampleBlock
Prueba a sumar dos cadenas alfanuméricas 
\end_layout

\begin_layout ExampleBlock
Da igual usar comillas simples o dobles
\end_layout

\end_deeper
\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Frame
\begin_inset Argument 4
status open

\begin_layout Plain Layout
Variables, escribir, tipo y referencias
\end_layout

\end_inset


\end_layout

\begin_layout Frame
Para memorizar un dato utilizamos variables, cada variable tiene su nombre
 (identificador) y su valor.
\end_layout

\begin_deeper
\begin_layout ColumnsTopAligned

\end_layout

\begin_deeper
\begin_layout Column
5cm
\end_layout

\begin_layout ExampleBlock
\begin_inset Argument 2
status open

\begin_layout Plain Layout
asignación
\end_layout

\end_inset


\end_layout

\begin_layout ExampleBlock
Prueba a poner:
\end_layout

\begin_layout ExampleBlock

\end_layout

\begin_layout ExampleBlock
i = 7
\end_layout

\begin_layout ExampleBlock
x = 3.3
\end_layout

\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout ExampleBlock
\begin_inset Argument 2
status open

\begin_layout Plain Layout
escribir
\end_layout

\end_inset


\end_layout

\begin_layout ExampleBlock

\end_layout

\begin_layout ExampleBlock
print(i)
\end_layout

\begin_layout ExampleBlock
print(x)
\end_layout

\begin_layout ExampleBlock

\end_layout

\begin_layout ExampleBlock
x = x + 7.7
\end_layout

\begin_layout ExampleBlock
print(x)
\end_layout

\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout Column
5cm
\end_layout

\begin_layout ExampleBlock
\begin_inset Argument 2
status open

\begin_layout Plain Layout
comprobar tipo
\end_layout

\end_inset


\end_layout

\begin_layout ExampleBlock
type(i)
\end_layout

\begin_layout ExampleBlock
type(x)
\end_layout

\begin_layout ExampleBlock

\end_layout

\begin_layout ExampleBlock

\end_layout

\begin_layout Standard
\begin_inset Separator plain
\end_inset


\end_layout

\begin_layout ExampleBlock
\begin_inset Argument 2
status open

\begin_layout Plain Layout
comprobar referencia
\end_layout

\end_inset


\end_layout

\begin_layout ExampleBlock
id(i)
\end_layout

\begin_layout ExampleBlock
i = 99
\end_layout

\begin_layout ExampleBlock
id(i)
\end_layout

\end_deeper
\end_deeper
\end_body
\end_document
