
# MÓDULO I. Introducción

1. Pythonic way of coding (1h) 
2. Entorno de trabajo (1h) 
3. Elementos básicos de programación (1h)

# MÓDULO II. Programación en Python 

4. Datos compuestos: listas, tuplas, diccionarios (3h)
5. Funciones y funciones como objetos de primera clase (3h)
4. Programación orientada a objetos (2h) 
5. Estructuras de datos y algoritmos (iteradores, generadores, ...) (1h) 

# MÓDULO III. Aplicaciones

6. Modulos y paquetes de terceras partes (2h)
7. Analisis de datos con Pandas (2h)
8. Representación gráfica con matplotly (2h)
