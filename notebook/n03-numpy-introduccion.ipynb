{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Vectores y numpy\n",
    "\n",
    "## Introducción\n",
    "\n",
    "Python, como lenguaje, no tiene vectores propios. El lenguaje sin librerías adicionales tiene las listas (`list`) como secuencia lineal de uso básico y generalizado. Sin embargo, estas listas tienen alguna características que no siempre es deseable:\n",
    "\n",
    "* Para empezar, hay que saber que, aunque se llamen listas, en realidad su implementación (al menos en CPython, el interprete de python más común) es un array de referencias. En realidad nos lo podemos imaginar en C como un array de punteros a `void*`. En todo caso estrictamente hablando no es una lista, sino un array, pero donde los elementos del array no ocupan posiciones contiguas en memoria. \n",
    "* Su interfaz permite hacer operaciones que no siempre son eficientes desde el puntos de vista de la velocidad del programa o del uso de memoria. \n",
    "* Pero su interfaz no tiene las operaciones habituales y propias del álgebra lineal y que suelen usarse como base para todo tipo de análisis matemático (productos escalares, producto de matrices, etc.). \n",
    "\n",
    "Si prescindimos de los detalles técnicos... Una `list` no sirve para guardar datos para análisis de datos intensivo (del tipo necesario en *big data*).\n",
    "\n",
    "Y por eso se introduce *Numpy*, una librería que, fundamentalmente, aporta vectores eficientes al lenguaje.\n",
    "\n",
    "De la página [web de Numpy](https://numpy.org/), *Numpy* proporciona: matrices N-dimensionales, funciones para operar con dichas matrices, incluso mezclando distintos tamaños (*broadcasting*), funciones del álgebra lineal, transformadas de Fourier y números aleatorios. Adicionalmente añade tipos de datos numéricos de tamaño distinto al básico, por ejemplo, enteros de 16 bits (int16), complejos y otras utilidades. \n",
    "\n",
    "*Numpy* se usa como parte, más o menos integrada, de los paquetes de análisis numéricos que se pueden consultar en [Web de Scipy](https://scipy.org/). \n",
    "\n",
    "## Diferencias entre numpy.array y list\n",
    "\n",
    "Antes de trabajar con *Numpy* vamos a ver algunos ejemplos que ilustran las diferencias esenciales entre `list` y `numpy.array`. Estas diferencias son cuestiones de detalle que normalmente pasan desapercibidas al usuario/programador ocasional. De hecho, normalmente no tiene ningún impacto en el desarrollo de la mayoría de programas, sin embargo si son interesantes, especialmente para poder explicar algunos comportamientos y funcionalidades (como los conceptos de *deep copy*, *shallow copy* o *view*). \n",
    "\n",
    "Como complemento previo se puede consultar la documentación oficial. Podéis encontrar tutoriales y documentación adicional en: [Documentación Numpy](https://numpy.org/devdocs/user/quickstart.html). Pero no es imprescindible, simplemente vamos a tratar de leer y entender algunos ejemplos sencillos. \n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[2.8, 4.5, -3.3, 7]"
      ]
     },
     "execution_count": 1,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "# Importamos Numpy como todo el mundo:\n",
    "import numpy as np\n",
    "\n",
    "# Definimos una lista (python lists):\n",
    "\n",
    "lista = [ 2.8, 4.5, -3.3, 7 ]\n",
    "\n",
    "lista\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([ 2.8,  4.5, -3.3,  7. ])"
      ]
     },
     "execution_count": 2,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "\n",
    "# Y ahora un vector de numpy\n",
    "\n",
    "vector = np.array([ 2.8, 4.5, -3.3, 7])\n",
    "\n",
    "vector\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Hasta ahora parecen bastante iguales, ambas estructuras se forman de manera parecida, en el `numpy.array` hay que poner su nombre para que cree, pero por lo demás no parece muy diferente. Aún así nos podemos fijar en una sutil diferencia: en la lista el 7 aparece escrito sin el punto decimal, es un entero, sin embargo en el *array* se muestra como 7. ¡Es un coma flotante!\n",
    "\n",
    "Vamos a ver un ejemplo un poco más dificil, vamos a formar el equivalente a una matriz: para las listas formamos una lista de listas, la vamos a llamar `listas_2`; sin embargo para *numpy* volvemos a escoger el mismo tipo de dato. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[[99, 4.5, -3.3, 7], [99, 4.5, -3.3, 7]]"
      ]
     },
     "execution_count": 3,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "listas_2 = [ lista, lista ]\n",
    "\n",
    "lista[0] = 99\n",
    "\n",
    "listas_2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[ 2.8,  4.5, -3.3,  7. ],\n",
       "       [ 2.8,  4.5, -3.3,  7. ]])"
      ]
     },
     "execution_count": 4,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "matriz = np.array([vector, vector])\n",
    "\n",
    "vector[0] = 99\n",
    "\n",
    "matriz"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "¿Qué ha pasado?\n",
    "\n",
    "Si nos vamos al detalle, al formar la `listas_2`, hemos *guardado dos referencias del mismo objeto* `lista`. Por eso si cambiamos algún elemento de ese objeto `listas_2` cambia, al fin y al cabo, lo contiene.\n",
    "\n",
    "Sin embargo, `numpy.array` no guarda referencias sino valores y por tanto eso no ocurre. Al cambiar el valor del vector, la matriz no cambia. \n",
    "\n",
    "Por si acaso, comprobemos que vector si ha cambiado..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([99. ,  4.5, -3.3,  7. ])"
      ]
     },
     "execution_count": 5,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "\n",
    "vector\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Copiar o no copiar ¡Esa es la cuestión!\n",
    "\n",
    "En ocasiones resulta difícil interpretar una sentencia como ésta:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[99, 4.5, -3.3, 7]"
      ]
     },
     "execution_count": 6,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "fila1 = listas_2[0]\n",
    "\n",
    "fila1 "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "¿Es `fila1` una nueva variable? La mayor parte de las veces la respuesta es *no*. Básicamente, lo que se hace es poner una nueva referencia al primer elemento de la lista. Vamos a comprobarlo, vamos a modificar `fila1` y veremos si se modifica `listas_2`\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[[-11.11, 4.5, -3.3, 7], [-11.11, 4.5, -3.3, 7]]"
      ]
     },
     "execution_count": 7,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "fila1[0] = -11.11\n",
    "\n",
    "listas_2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Parece que, efectivamente se modifica. Pero, ¿y si no queremos? ¿Cómo podemos lograr que se copien los valores? \n",
    "\n",
    "Se pueden encontrar dos respuestas sencillas en muchas páginas de ayuda de python con *copy* o *slice*, veamos cómo se puede hacer:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[[-11.11, 4.5, -3.3, 7], [-11.11, 4.5, -3.3, 7]]"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "fila1 = listas_2[0].copy()\n",
    "\n",
    "fila1[0] = 22.2\n",
    "\n",
    "fila2 = listas_2[0][:]\n",
    "fila2[0] = 22.2\n",
    "\n",
    "listas_2"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "¡Y ahora ninguno de los valor en la primera posición de cada fila se ha modificado! Se dice que tanto *copy* como *slice* producen una *shallow copy*. Por ahora hemos visto que si se han copiado y ya veremos qué significa *shallow*.\n",
    "\n",
    "El objeto *slice* son los dos puntos que aparecen en el segundo corchete de la línea 5. Un objeto `slice` se forma con 3 números, el inicio, el final y el paso. Si se pone el paso hay que poner otros dos puntos (por ejemplo: `0:5:2`), si el paso no se indica se supone que es `1`. Si el inicio no se pone se supone que es el primero, si no se pone el final se supone que es el último. Los objectos `slice` se pueden utilizar en lugar de los índices enteros para seleccionar rangos de elementos en estructuras de datos lineales. \n",
    "\n",
    "¿Qué ocurre si hacemos todo esto con *numpy*? Veamos:\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[-11.11,   4.5 ,  -3.3 ,   7.  ],\n",
       "       [  2.8 ,   4.5 ,  -3.3 ,   7.  ]])"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "fila1 = matriz[0]\n",
    "fila1[0] = -11.11\n",
    "\n",
    "matriz"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "¡Ocurre lo mismo! Y esto debería ser una sorpresa porque una matriz no guarda referencias a las filas, entonces ¿Qué es lo que ha ocurrido?\n",
    "\n",
    "En realidad, cuando *seleccionamos* una parte de un *array* de numpy creamos una *vista* de esa matriz, una vista y una referencia son conceptos relacionados porque la vista guarda una referencia al objeto original al que está *mirando* y por eso la `matriz` se modifica al modificar la variable `fila1`. \n",
    "\n",
    "Las diferencias entre vista y referencia son:\n",
    "\n",
    "* En la vista, la referencia es al conjunto de las componentes del array, \n",
    "* aunque no se pueda acceder a todos esas componentes.\n",
    "* Además, solo se comparten las componentes, otras cosas como la forma de la matriz pueden cambiar.\n",
    "* Mientras, en el caso de la lista y la referencia, solo se accede una parte del objeto original, pero a todos los datos de ese elemento. \n",
    "\n",
    "Para hacer una copia tenemos que ponerlo explicítamente, igual que antes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "array([[-11.11,   4.5 ,  -3.3 ,   7.  ],\n",
       "       [ 22.2 ,   4.5 ,  -3.3 ,   7.  ]])"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "fila1 = matriz[0].copy()\n",
    "\n",
    "fila1[0] = 22.2\n",
    "\n",
    "fila2 = matriz[1,:]\n",
    "fila2[0] = 22.2\n",
    "\n",
    "matriz"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pero ¡Ahora ha cambiado la segunda fila de matriz!\n",
    "\n",
    "Si, esta es otra diferencia entre `list` y `numpy.array`, el comportamiento frente a los *slices* y frente a los índices cambia ligeramente:\n",
    "\n",
    "* *Slice* no hace copias.\n",
    "* A todo esto, los objectos de *slice* en *numpy* incluyen el último elemento y en python a secas se excluye. \n",
    "* Cuando hay índices múltiples, los índices se ponen todos dentro del corchete y se separan con comas (como si fuesen una *tupla*). \n",
    "\n",
    "Tenéis otro(s) documento(s) con ejercicios más prácticos. En notebook aparte, por ahora:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Que sea leve la cuarentena y cuidaros mucho\n"
     ]
    }
   ],
   "source": [
    "# Esto es todo todo todo amigos\n",
    "\n",
    "print(\"Que sea leve la cuarentena y cuidaros mucho\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
