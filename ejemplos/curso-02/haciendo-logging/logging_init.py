
import logging
import configparser

parser = configparser.RawConfigParser()

parser.read("config.ini")

print("Opciones leídas del archivo de configuración")
print("--------------------------------------------")
print()
print(parser.items("Logging"))

options = {}
for key, value in parser.items("Logging"):
    options[key] = value
    
# Se puede hacer mejor?? Si =>
print()
option_alt = dict(parser.items("Logging"))
print(option_alt)
print()

# Un diccionario establece una correspondencia, en este caso entre
# una cadena y su correspondiente constante (numérica).
change = {
    "DEBUG" : logging.DEBUG,
    "INFO" : logging.INFO,
    "WARNING" : logging.WARNING,
    "ERROR" : logging.ERROR,
    "CRITICAL" : logging.CRITICAL
}
print()

print("Esta es la correspondencia de la cadena con el nivel de log: ")
print("-------------------------------------------------------------")
print()
print(change)
print()

print("Y este el diccionario modificado: ")
print("-------------------------------------------------------------")
print()

options["level"] = change[options["level"]]
print(options)
print()

logging.basicConfig(**options)

# logging.basicConfig(format="%(asctime)s:%(levelname)s:%(message)s", level=logging.DEBUG)
# logging.basicConfig(level=logging.DEBUG, format='%(relativeCreated)6d %(threadName)s %(message)s')

logging.info("Hola")

