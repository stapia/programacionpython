
import logging as log
import requests as rq

pdf = rq.get("http://138.100.77.41/python/CARTEL_PYTHON.pdf")

print("Intentando descargar cartel....")

if pdf:
    with open("pdf-descargado", "wb") as f:
        f.write(pdf.content)
        print("Cartel descargado")
        print()
else:
    log.error("Response no es OK al descargar CARTEL_PYTHON")

print("Intentando descargar archivo json.....")

response = rq.get("https://raw.githubusercontent.com/NREL/EnergyPlus/develop/weather/master.geojson")

if response:
    with open("weather.json","w") as f:
        f.write(response.text)
        print("weather.json descargado")
        print()
else:
    log.error("Response no es OK al descargar weather.json")
    

# La respuesta es un archivo json que podemos transformar en una 
# estructura de datos a base de list y dict
toc = response.json()

print("Distintas partes del json:")
print()
print("toc['type']", toc['type'])
print()
print("toc['features'][0]['properties']['epw']", toc['features'][0]['properties']['epw'])
print()

print("Y finalmente la URL que buscamos: ")
url = toc['features'][0]['properties']['epw'].replace("<a href=", "").replace(">Download Weather File</a>","")
print(url)

response = rq.get(url)

if response:
    with open("kota.epw","wb") as f:
        f.write(response.content)
        print("Archivo descargado")
else:
    log.error("Response no es OK")
