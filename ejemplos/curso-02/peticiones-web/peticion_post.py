
import logging as log
import requests as rq

# Se puede hacer otro tipo de peticiones, por ejemplo, un post:
try:
    payload = "La cadena que voy a guardar"
    url = f'http://138.100.77.111:8111/api/str/12'
    response = rq.request('POST', url, data=payload)
except Exception as err:
    log.error(f"Excepcion al procesar primer POST: {err}")
    
# Se puede ver el resultado en: http://138.100.77.111:8111/api/str/12

# Este otro va a fallar:
try:
    payload = "La cadena que voy a guardar"
    url = f'http://138.100.77.250'
    response = rq.request('POST', url, data=payload)
except Exception as err:
    log.error(f"Excepcion POST: {err}")
