
function procesarDatos() {
    let form = document.getElementById("input-data");
    var formData = new FormData(form);
    console.log(Array.from(formData.entries()));

    $.ajax({
        url: "/api/procesar",
        type: "POST",
        data: formData,
        processData: false,  // tell jQuery not to process the data
        contentType: false,   // tell jQuery not to set contentType
        success: function (result) {
            console.log("Exito: %o", result);
            $("#result").html(`${JSON.stringify(result)}`);
        },
        error: function (result) {
            console.log("Error: %o", result);
            $("#result").html(`${JSON.stringify(result)}`);
        }
    });
}

function init() {
    console.log("Init");
    $("#procesarDatos").click(procesarDatos);
}

$(document).ready(init);

