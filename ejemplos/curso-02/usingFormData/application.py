#!/usr/bin/python3

from flask import Flask, request
from flask_restful import Api, Resource

from Procesando import *

url_prefix = ""
if __name__ == '__main__':
    url_prefix = "/api"

app = Flask(__name__)
api = Api(app)


api.add_resource(Procesando, url_prefix + '/procesar')

if __name__ == '__main__':
    app.run(debug=True)

application = app
