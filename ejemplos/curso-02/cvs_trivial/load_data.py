
import pandas as pd
import plotly.graph_objects as go

datos = pd.read_csv('hoja.csv', delimiter = ',')

print('Mostrando los datos')
print('-------------------')
print(datos)

print('Mostrando la media de los datos')
print('-------------------------------')
print(datos.mean())

fig = go.Figure(
    data = [ 
        go.Scatter(x = datos['X'], y = datos['A']),
        go.Scatter(x = datos['X'], y = datos['B']),
        go.Scatter(x = datos['X'], y = datos['C'])
    ],
    layout_title_text="Linea"
)

fig.show()
