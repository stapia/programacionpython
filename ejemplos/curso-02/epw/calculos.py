
import pandas as pd
import numpy as np

import leer_epw as epw
import poner_fecha 

df = epw.abrir_epw('IND_Kota.424520_ISHRAE.epw')
df = poner_fecha.poner_fecha(df)

# print(df.iloc[20:28,0:4])

# df.info()

# Agrupa por días:
por_dias = df.groupby(df.index.map(lambda x: f"{x.month:02d}/{x.day:02d}"))

dias = pd.DataFrame(columns=['day', "DryBulb (ºC)", "DewPoint (ºC)"])
# dias.info()

for dia, datos in por_dias:
    dias = dias.append( { 'day': dia, "DryBulb (ºC)": datos["DryBulb (ºC)"].mean(), "DewPoint (ºC)": datos["DewPoint (ºC)"].mean() }, ignore_index=True)

print(dias.iloc[30:50])
# print(dias.loc["08/25"])
