""" Lee un archivo EPW quitando lo que sobra
Consultar parametros de read_csv en:
    https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.read_csv.html
"""

import pandas as pd

def select_cols(name):
    """ Devuelve True para las columnas a cargar

    Un callable es una función que se pasa como argumento en otra función.
    Se tiene que indicar los parámetros necesarios y el retorno adecuado.
    """
    return name != 5 and name != 4 # Quito dos columnas "inútiles"

def abrir_epw(filename):
    """ Abre el EPW con las opciones necesarias

    Buscar significado de los parámetros en la referencia.
    """
    with open(filename) as epw:
        #df = pd.read_csv(epw, skiprows=8, nrows=10, header=None, names=range(35))
        df = pd.read_csv(epw, skiprows=8, header=None, names=range(35), usecols=select_cols)
        return df
    
if __name__ == "__main__":     
    fd = abrir_epw('IND_Kota.424520_ISHRAE.epw')
    # iloc sirve para seleccionar una parte del archivo mediante i,
    # es decir, indice (empieza en 0).
    
    # Comprobar siempre "principio"
    print(fd.head().iloc[:,0:7])
    # Y final
    print(fd.tail().iloc[:,30:35])
    # Como los valores son iguales pongo otras columnas:
    print(fd.tail().iloc[:,0:7])