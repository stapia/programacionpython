""" Cambia las tres primeras columnas de un DataFrame

Consultar ejemplo en:
    https://realpython.com/python-pandas-tricks/#4-create-a-datetimeindex-from-component-columns
"""
import pandas as pd

def poner_fecha(df):
    """ Sustituye las 4 primeras columnas por la fecha y, luego, las elimina

    Esta función tiene un poco de todo: renombra columnas, cambia valores, crea una
    nueva columna (que es el "índice") y elimina. 
    """
    # Los nombres originales eran números
    df.rename({ 0: "year", 1: "month", 2: "day", 3: "hour"}, axis='columns', inplace=True)
    
    # Tenemos que cambiar los valores de la columna de horas por uno menos
    df["hour"] = df["hour"].apply( lambda x : x - 1 )
    
    # Para que datetime funcione las columnas tienen que estar etiquetadas con year, month, ..
    datecols = ['year', 'month', 'day', 'hour' ]
    
    # index es el nombre de la fila, es decir, hemos conseguido una serie temporal
    df.index = pd.to_datetime(df[datecols])
    
    # Quitamos las columnas que sobran (axis se ve mejor que significa si ponemos columns)
    df = df.drop(datecols, axis="columns").squeeze()
    
    labels = { 6 : "DryBulb (ºC)", 7 : "DewPoint (ºC)",  8 : "RelHum (%)", 9 : "Atm Pressure (Pa)" }
    df.rename(labels, axis='columns', inplace=True)
    
    return df

if __name__ == "__main__":
    import leer_epw as epw
    
    df = epw.abrir_epw('IND_Kota.424520_ISHRAE.epw')
    
    df = poner_fecha(df)
    
    # Compruebo en un sitio "critico": el cambio de un día al siguiente
    print(df.iloc[20:28,0:6])
