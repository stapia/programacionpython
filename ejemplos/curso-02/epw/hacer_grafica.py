
import plotly.graph_objs as go

import pandas as pd

def hacer_fig(df):
    
    layout_data = go.Layout(
        title = 'Año Meteorológico Tipo',
        yaxis = dict(title='Medidas'),
        xaxis = dict(title='Tiempo')
    )
    
    col_names = [ "DryBulb (ºC)", "DewPoint (ºC)", "RelHum (%)" ]
    
    graph_data = [ go.Scatter( x = df.index, y = df[col_name], name = col_name) for col_name in col_names ]

    fig = go.Figure( data = graph_data, layout = layout_data)

    return fig


if __name__ == "__main__":
    import leer_epw as epw
    import poner_fecha 
    
    df = epw.abrir_epw('IND_Kota.424520_ISHRAE.epw')
    df = poner_fecha.poner_fecha(df)

    print(df.iloc[20:28,0:4])
    
    fig = hacer_fig( df )
    fig.show()
    
