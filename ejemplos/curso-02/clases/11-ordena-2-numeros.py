#!/usr/bin/python3

# Ejemplo para ilustrar el uso de clases y objetos

# Las clases usualmente residen en librerías:
import tkinter as tk

# En la siguiente línea:
raiz = tk.Tk()

# i) raiz es el nombre del nuevo objeto,
# ii) tk es el espacio de nombres de tkinter (gui)
# iii) Tk es el nombre de una clase

# Los objetos tienen asociados métodos:  
raiz.title("Hola")
# Se llaman con el nombre de la variable objeto
# seguido de punto, el método y sus argumentos

# Creamos más objetos:
etiqueta1 = tk.Label(raiz, text="Número 1: ")
etiqueta2 = tk.Label(raiz, text="Número 2: ")
# Como se puede ver un objeto puede ser necesario
# para generar otro. Esta es la base del diseño
# en POO donde se analizan y estudian las relaciones
# entre clases y objetos

editable1 = tk.Entry(raiz)
editable2 = tk.Entry(raiz)

# Colocamos en rejilla:
etiqueta1.grid(row=0, column=0)
etiqueta2.grid(row=1, column=0)
editable1.grid(row=0, column=1)
editable2.grid(row=1, column=1)

# Acción del botón:


def ordena():
    # Los objetos tienen "estado", es decir,
    # el conjunto de valores de sus variables internas.
    # Se puede obtener ese estado a través de métodos o
    # directamente accediendo a sus atributos.
    x = float(editable1.get())
    y = float(editable2.get())
    # En este caso se obtiene el "estado" de los editables

    if x > y:
        # También se puede modificar el "estado" de un objeto
        # a través de sus métodos o atributos:
        editable1.delete(0, tk.END)
        editable1.insert(0, str(y))
        editable2.delete(0, tk.END)
        editable2.insert(0, str(x))
        # En este caso los métodos delete e insert permiten
        # modificar la cadena que ve el usuario en la pantalla


boton = tk.Button(raiz, text="Ordenar", command=ordena)
boton.grid(row=2, column=1)

# Esto va al final es un método para lanzar la
# aplicación. Es "bloqueante" solo termina cuando
# se cierra la aplicación. Por eso pone "loop"
raiz.mainloop()
