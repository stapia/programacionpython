#!/usr/bin/python3

import math

# Ejemplo para ilustrar la programación de class (parte 2)

# Repetimos la clase de antes
class Vector3:
    def __init__(self, x, y, z):
        self.coords = [x, y, z]

    def norma(self):
        suma = 0
        for x in self.coords:
            suma += x*x

        return math.sqrt(suma)

    def __lt__(self, other):
        return self.norma() < other.norma()

import tkinter as tk

# Vamos a añadir a la clase de antes una manera
# de definir sus datos desde la GUI

class VectorNumeros:

    def __init__(self, raiz, *args):
        if len(args) != 3:
            args = [0, 0, 0]

        # Se pueden usar clases propias como atributos
        self.v = Vector3(*args)

        # Y también ajenas!
        self.frame = tk.Frame(raiz)
        self.editables = [
            tk.Entry(self.frame) for x in args
        ]

        for i, edit in enumerate(self.editables):
            edit.grid(row=0, column=i)

    # Se pueden definir otros metodos, por ejemplo:

    def pack(self):
        self.frame.pack()

    # En esos ejemplos se puede cambiar el estado
    # del objeto o generar una salida.

    # Este método cambia el estado, transfiere
    # los datos de los editables al vector en memoria:
    def from_GUI_to_Data(self):
        updateCoords = [float(edit.get()) for edit in self.editables]
        self.coords = updateCoords
        return self.coords

    # o este que hace el camino contrario
    def from_Data_to_GUI(self):
        for x, edit in zip(self.coords, self.editables):
            edit.delete(0, tk.END)
            edit.insert(0, str(x))

raiz = tk.Tk()
raiz.title("Ordenando")

vectores = [VectorNumeros(raiz) for i in range(1, 5)]

for v in vectores:
    v.pack()

# Acción del botón:


def ordena():
    listV = [ v.from_GUI_to_Data() for v in vectores ]
    listV.sort()

    for v, vGUI in zip(listV, vectores):
        vGUI.coords = v
        vGUI.from_Data_to_GUI()

boton = tk.Button(raiz, text="Ordenar", command=ordena)
boton.pack()

# Esto va al final es un método para lanzar la
# aplicación. Es "bloqueante" solo termina cuando
# se cierra la aplicación. Por eso pone "loop"
raiz.mainloop()
