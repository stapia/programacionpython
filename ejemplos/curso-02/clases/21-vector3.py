#!/usr/bin/python3

# Ejemplo para ilustrar la programación de class (parte 1)

import math

# Vamos a crear una clase sencilla, un vector de números:


class Vector3:

    # Dentro de una clase se ponen métodos como este:
    def __init__(self, x, y, z):
        self.coords = [x, y, z]
        # Los atributos se definen dandoles
        # valores, se indican con `self.` y el
        # nombre del atributo

    # __init__ es un método especial porque siempre
    # se llama cuando se crea un nuevo objeto de la
    # clase que se esta definiendo.
    # Los métodos tienen como primer argumento
    # siempre `self`, es el objeto con el que hace la
    # llamada

    # Los métodos pueden servir para obtener información
    # del objeto, por ejemplo su norma:

    def norma(self):
        suma = 0
        for x in self.coords:
            suma += x*x

        return math.sqrt(suma)

    # O para indicar una relación de orden (less than)
    def __lt__(self, other):
        return self.norma() < other.norma()

    # O para modificar su estado interno, a través de
    # modificar los valores de los atributos del objeto:
    def incremento(self):
        self.coords[0] += 1

    def incremento_NoFunciona(self):
        for x in self.coords:
            x += 1

# Podemos crear un vector:
v = Vector3(3, 4, 5)  # LLama a __init__
print(v.coords)  # Se puede acceder directamente a los atributos

v.incremento() # Modifica coords
print(v.coords)  

v.incremento_NoFunciona() # ¿Por qué no cambian las coordenadas?
print(v.coords)

print("La norma es: ", v.norma())

# Se pueden usar para crear "estructuras de datos"
listaVectores = [
    v,
    Vector3(1.1, 1, 0.3),
    Vector3(10.1, 5, 8.3),
    Vector3(2.1, 7, 0.3),
]

listaVectores.sort() # ¡¡ Usa __lt__ !!

# Y queda:
for item in listaVectores:
    print("Norma: ", item.norma(), "vector: ", item.coords)

