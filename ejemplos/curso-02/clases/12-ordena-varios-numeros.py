#!/usr/bin/python3

# Ejemplo para ilustrar el uso de clases y objetos (parte 2)

import tkinter as tk

raiz = tk.Tk()
raiz.title("Ordenando")

# Los objetos se pueden gestionar dinámicamente,
# por ejemplo, generando listas de objetos:

etiquetas = [
    tk.Label(raiz, text=f"Número {i}: ")
    for i in range(1, 5)
]

editables = [tk.Entry(raiz) for i in range(1, 5)]

# Y, naturalmente, luego podemos hacer bucles con ellos:

for i, etiqueta in enumerate(etiquetas):
    # Colocamos en rejilla:
    etiqueta.grid(row=i, column=0)

for i, editable in enumerate(editables):
    # Colocamos en rejilla:
    editable.grid(row=i, column=1)

# Acción del botón:


def ordena():
    numeros = [float(editable.get())
               for editable in editables
               ]

    numeros.sort()

    for editable, x in zip(editables, numeros):
        editable.delete(0, tk.END)
        editable.insert(0, str(x))


boton = tk.Button(raiz, text="Ordenar", command=ordena)
boton.grid(row=len(editables), column=1)

# Esto va al final es un método para lanzar la
# aplicación. Es "bloqueante" solo termina cuando
# se cierra la aplicación. Por eso pone "loop"
raiz.mainloop()
