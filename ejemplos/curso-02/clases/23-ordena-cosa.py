#!/usr/bin/python3

# Vamos a hacer una clase propocione una forma de
# generar una aplicación para hacer la ordenación
# de cualquier cosa.

import tkinter as tk
import datetime

class OrdenarCosas:
    def __init__(self, Cosa):
        print("Solo por comprobar esto: ", Cosa)
        # Inicializa App:
        self.raiz = tk.Tk()
        self.raiz.title("Ordenando Cosas")

        # Crea las cosas:
        self.listCosas = [Cosa(self.raiz) for i in range(1, 4)]

        for cosa in self.listCosas:
            cosa.pack()

        self.boton = tk.Button(self.raiz, text="Ordenar",
                               command=lambda: self.ordena())
        self.boton.pack()

    def run(self):
        self.raiz.mainloop()

    def ordena(self):
        listData = [cosa.from_GUI_to_Data() for cosa in self.listCosas]
        listData.sort()

        for data, gui in zip(listData, self.listCosas):
            gui.from_Data_to_GUI(data)

# Y ahora definimos una "cosa", por ejemplo, una fecha
class Fecha:

    def __init__(self, raiz):
        self.frame = tk.Frame(raiz)
        self.editables = []

        for i, texto in enumerate([ 'año', 'mes', 'día' ]):
            label = tk.Label(self.frame, text=texto)
            label.grid(row=0, column=i)
            e = tk.Entry(self.frame)
            e.grid(row=1, column=i)
            self.editables.append(e)

    def pack(self):
        self.frame.pack()

    def from_GUI_to_Data(self):
        datos = [ int(e.get()) for e in self.editables ]
        return datetime.date(*datos)

    def from_Data_to_GUI(self, una_fecha):
        for edit in self.editables:
            edit.delete(0, tk.END)

        E = self.editables
        E[0].insert(0, str(una_fecha.year))
        E[1].insert(0, str(una_fecha.month))
        E[2].insert(0, str(una_fecha.day))
        

app = OrdenarCosas(Fecha)
app.run()

