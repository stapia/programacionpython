
import math

x = float(input("Introduce valor de x:"))

x = math.radians(x)

y = math.sin(x) if math.sin(x) > math.cos(x) else math.cos(x)

print("x = ", x, " y = ", y)
