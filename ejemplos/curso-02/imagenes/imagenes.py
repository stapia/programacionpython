
from PIL import Image, ImageDraw, ImageFilter
import glob

imagenes = glob.glob('*.jpg')

for file in imagenes:
    with Image.open(file) as im:
        im.show()
        draw = ImageDraw.Draw(im)
        draw.line((0, 0) + im.size, fill=128)
        #im = im.rotate(45)
        #im = im.filter(ImageFilter.EDGE_ENHANCE)
        im = im.filter(ImageFilter.SMOOTH)
        im.show()

