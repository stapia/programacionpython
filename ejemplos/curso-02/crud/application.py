#!/usr/bin/python3

import datetime
import redis

from flask import Flask, render_template
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)

url_prefix = ""
if __name__ == '__main__':
    url_prefix = "/api"

# Este es un ejemplo trivial para ver que funciona:
class Echo(Resource):
    def get(self, mensaje):
        return f"Echo: '{mensaje}'" 

api.add_resource(Echo, url_prefix + '/echo/<mensaje>')

# Abrir base de datos
rdb = redis.Redis()

# Esto es lo que hay que cambiar!!!

from BaseDeDatos import BaseDeDatos # El nombre del archivo y la clase que vosotros creeis

# En esta linea haya que sustituir el nombre de la clase (en este caso se llama BaseDeDatos) y la URL
api.add_resource(BaseDeDatos, url_prefix + '/db/<int:the_id>', resource_class_kwargs = { 'db' : rdb} )

# Y esto se deja tal cual 
@app.route(url_prefix + "/default")
def por_defecto():
    return render_template("reloj.html", reloj = datetime.datetime.now())

if __name__ == '__main__':
    echo = Echo('/echo/<mensaje>')
    print(echo.get("Hola soy yo"))
    
    app.run(debug=True)

application = app
