
from flask import Flask, request
from flask_restful import Resource

# No hace falta importar porque no se usa explicitamente.
# import redis


class BaseDeDatos(Resource):

    def __init__(self, **kwargs):
        self.db = kwargs['db']

    def get(self, the_id):
        result = self.db.get(the_id)
        if result:
            return result.decode('utf-8')
        else:
            return "No existe"

    def post(self, the_id):
        valor = request.data
        print(f"clave {the_id} / valor {valor}")
        return self.db.set(the_id, valor)

    def put(self, the_id):
        valor = request.data
        print(f"clave {the_id} / valor {valor}")
        return self.db.set(the_id, valor)

    def delete(self, the_id):
        return self.db.delete(the_id)
