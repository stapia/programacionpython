
import plotly.graph_objects as go
import math

sides = 8

x_vertex = [0]*(sides+1)
y_vertex = [0]*(sides+1)
i = 0

while i <= sides:
	angle = math.radians(360 / sides * i)
	x_vertex[i] = math.cos(angle)
	y_vertex[i] = math.sin(angle)
	i += 1

fig = go.Figure(
    data = [ go.Scatter(
		x = x_vertex,
		y = y_vertex
	)],
	layout = go.Layout(
		autosize=False,
		width=500,
		height=500),
    layout_title_text="Poligono"
)
fig.show()
