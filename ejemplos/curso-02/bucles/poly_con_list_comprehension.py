
import plotly.graph_objects as go
import math

sides = 8

# Usando List Comprehension 

# Es una sintaxis abreviada del for anterior...

angles = [ math.radians(360 / sides * i) for i in range(sides+1)]
x_vertex = [ math.cos(angle) for angle in angles ]
y_vertex = [ math.sin(angle) for angle in angles ]

fig = go.Figure(
    data = [ go.Scatter(
		x = x_vertex,
		y = y_vertex
	)],
	layout = go.Layout(
		autosize=False,
		width=500,
		height=500),
    layout_title_text="Poligono"
)
fig.show()
