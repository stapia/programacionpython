
i = 1

while i < 100:
	print(i)
	i += 2
	
cadenas = [ "hola", "alumnos", "python" ]

# Un bucle while puede servir para recorrer una lista.
i = 0
while i < len(cadenas):
	print(cadenas[i])
	i += 1

# Pero ¡¡¡Es mucho mejor evitar los índices!!!
for cad in cadenas:
	print(cad)

# Y si los necesitamos los podemos obtener
for index, cad in enumerate(cadenas):
	print(index, ":", cad)
	
# Suele haber una solución para evitar los índices,
# incluso si queremos iterar en dos listas a la vez...
numeros = [ 10, -3, 6 ]	

for cad, num in zip(cadenas, numeros):
	print(cad, "->", num)
