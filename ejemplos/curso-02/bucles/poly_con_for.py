
import plotly.graph_objects as go
import math

sides = 8

x_vertex = []
y_vertex = []

for i in range(sides+1):
	angle = math.radians(360 / sides * i)
	x_vertex.append(math.cos(angle))
	y_vertex.append(math.sin(angle))

fig = go.Figure(
    data = [ go.Scatter(
		x = x_vertex,
		y = y_vertex
	)],
	layout = go.Layout(
		autosize=False,
		width=500,
		height=500),
    layout_title_text="Poligono"
)
fig.show()
