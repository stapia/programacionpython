
import plotly.graph_objects as go
import math

x_vertex = [ 0.0,  0.3, 0.6, 0.9, 1.2,  1.5, 1.8, 2.1 ]
y_vertex = [ 1.0, -2.3, 4.6, 6.9, 0.2, -0.5, 6.8, 2.1 ]

fig = go.Figure(
    data=[go.Scatter(
		x = x_vertex,
		y = y_vertex
	)],
    layout_title_text="Linea"
)
fig.show()
