#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  9 18:19:50 2020

@author: Santiago Tapia Fernandez
"""

# Escribir una función que devuelve
# si n es primo al estilo "C"

def esPrimo(n):
    
    primo = True
    i = 2
    
    while primo and i < n:
        primo = n % i != 0
        i = i + 1
        
    return primo

if __name__ == "__main__":
    print(esPrimo(12))
    print(esPrimo(11))
    print(esPrimo(48))
    print(esPrimo(47))   
