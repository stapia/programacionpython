#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  9 18:19:50 2020

@author: Santiago Tapia Fernandez
"""

def esPrimo(n, listaPrimo = None):
    """\
    esPrimo devuelve si el argumento
    se un número primo (True) o no (False)
    """
    if listaPrimo != None:
        secuenciaPrimos = listaPrimo
    else:
        secuenciaPrimos = range(2, n)
        
    for i in secuenciaPrimos:
        if n % i == 0: return False
        
    return True
    
if __name__ == "__main__":
    print(esPrimo(12))
    print(esPrimo(11))
    print(esPrimo(48))
    print(esPrimo(47))   
    print(esPrimo(48, [2, 3, 5, 7, 11]))
    print(esPrimo(47, [2, 3, 5, 7, 11]))   
