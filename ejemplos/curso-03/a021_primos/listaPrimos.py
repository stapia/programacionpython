#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  9 18:49:01 2020

@author: alumno
"""

import primosClasico as p

def primosHasta(n):
    """\
    Se devuelve la lista de los números 
    primos comprendidos entre 2 y n
    """
    return [ i for i in range(2, n+1) if p.esPrimo(i) ]

if __name__ == "__main__":
    print( primosHasta(46) )
    print( primosHasta(47) )
    print( primosHasta(48) )