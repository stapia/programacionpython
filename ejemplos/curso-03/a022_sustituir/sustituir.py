#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar  9 19:35:26 2020

@author: alumno
"""

def sustituir(lista, **kwargs):
    """\
    Sustituye en una lista un keyword llamado
    x por otro y"""
    
    x = kwargs["x"] 
    y = kwargs["y"]
    
    return [ item if item != x else y
             for item in lista ]

if __name__ == "__main__":
    lista =  [ 1, 2, 3, 4, 5]
    
    listaS = sustituir(lista, x = 1, y = 99)
    print(listaS)
    
    opciones = { "x": 3, "y": 77 }
    listaS = sustituir(lista, **opciones)
    print(listaS)

    listaNo = sustituir(lista, y = 99)
    print(listaNo)

     