
# Lo mismo con funciones y un diagrama de "tarta"

import matplotlib.pyplot as plt

data = { "hola": 12, 
         "buenos días" : 30, 
         "buenas tardes": 20, 
         "buenas noches": 15 }

# lambda pct: f"veces: {pct:.1f}%"
def hacer_etiqueta(pct):
    return f"veces: {pct:.1f}%"

otros = [ "H", "BD", "BT", "BN" ]

# Por qué no puedo hacer:
# counter = 0 NO
# counter = ( 0 ) NO 
# counter = [ 0 ] SI
counter = { "i": 0 } # SI

def etiqueta(pct, otros, counter):
    res = f"{otros[counter['i']]}: {pct:.1f}%"
    counter['i'] = counter['i'] + 1
    # con número sería counter = counter + 1
    return res

otras = [ "Uno", "Dos"]
indice =  { "i": 0 }
print( etiqueta( 21, otras, indice ) )
print(f"El indice es: {indice}")

print( etiqueta( 77, otras, indice ) )
print(f"El indice es: {indice}")

option = 3

if option == 0:
    plt.pie(data.values(), labels=data.keys())
elif option == 1:
    # autopct es un callable
    plt.pie(data.values(), labels=data.keys(), autopct = hacer_etiqueta )
    # Internamente que pie llama al callable:
    #    autopct(19.5)
elif option == 2:
    # lambda es un callable que se define sobre la marcha
    plt.pie(data.values(), labels=data.keys(), autopct = lambda pct: f"veces: {pct:.1f}%" )
    # Internamente que pie llama al callable:
    #    autopct(19.5)
else: 
    # No es una opción porque etiqueta necesita sus 3 argumentos
    # plt.pie(data.values(), labels=data.keys(), autopct = etiqueta(10, otros, counter) )
    plt.pie(data.values(), labels=data.keys(), autopct = lambda pct: etiqueta(pct, otros, counter) )
    # Internamente se llama a:
    # autopct(100. * frac) <=> etiqueta(pct, otros, counter)
    
plt.savefig("pie.png")
plt.show()
