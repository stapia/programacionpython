
import numpy as np

x = [23, 24, 25, 26, 27, 28 ]
x.append("hola")
print("La x es:")
print(x)

a = np.arange(23, 35)
print("Vector a:")
print(a)

# Template string 
print(f"El valor a[2] = {a[2]}")

# Esto es inplace
a.resize(4,3)
print("Matriz a:")
print(a)

# Los índices son tuplas (no hace falta el paréntesis)
a[2,1] = 10
a[3,2] = 8
print("Matriz a modificada:")
print(a)

# Esto también inplace
a.sort() # callable
print("Matriz a (ordenada):")
print(a)

sorted_a = np.sort(a, axis=0)
print("Otra ordenacion:")
print(sorted_a)

b = np.random.randint(2, high = 20, size = 12)
print(b)

# Funciones element wise 
unos = np.ones(12)
doses = 2 * unos
doses[0] = 3
c = doses * b
print("b multicado por doses:")
print(c)

# O del algebra (producto escalar)
print(f"b producto escalar con doses: {np.dot(doses, b)}")

# U otros calculos:
print(f"suma acumulada de b: {np.cumsum(b)}")

# Una vista (view)... Cambia la manera de usar los índices 
# pero no copia los datos en otro sitio
vista_b = b[2:6] # Slicing!
print("Vista de b:")
print(vista_b)

vista_b[0] = 999
vista_b[2] = 999
print("b:")
print(b)

# Esto no es inplace, devuelve una copia de b 
D = b.reshape(3, 4)
print("Matriz D:")
print(D)
print("b:")
print(b)

# Notación basada en función
maxR = np.argmax(D, axis = 0)
print(maxR)

# Notación basada en objetos
maxR = D.argmax(axis = 1)
print(maxR)
