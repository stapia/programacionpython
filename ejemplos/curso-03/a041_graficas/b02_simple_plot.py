
import matplotlib.pyplot as plt
import numpy as np

# Datos con numpy
abcisas = np.arange(0.0, 4.0, 0.01)
y_seno = np.sin(2 * np.pi * abcisas)
y_coseno = np.cos(2 * np.pi * abcisas)

# Importante: Genera objetos
fig, ax = plt.subplots()

# Y se manipulan con sus "metodos"
ax.plot(abcisas, y_seno)

# Información adicional con parámetros opcionales o con nombre!
linea_cos = ax.plot(abcisas, y_coseno, "o:", label = "coseno", markersize = 1.8)

# Y/o guardando el retorno y usando sus métodos:
linea_cos[0].set_color('#55ff66') # linea_cos es una lista de 1 elemento

ax.set(xlabel='abcisas', ylabel='Y (valores)', title='Seno y coseno')
ax.grid() # Sin información adicional

# Con información adicional hay que consultar la referencia ...
ax.legend(loc = (0.2,1.05))

fig.savefig("trigo1.svg")
plt.show()