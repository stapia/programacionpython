import plotly.graph_objects as go
import numpy as np

t = np.linspace(0, 10, 100)

fig = go.Figure()

# Genera la traza que es una nube de punto (Scatter)
nube_puntos = go.Scatter(
    x=t, y=np.sin(t),
    name='sin',
    mode='markers',
    marker_color='rgba(152, 0, 0, .8)')

# Añade la nube a la figura
fig.add_trace(nube_puntos)

fig.add_trace(go.Scatter(
    x=t, y=np.cos(t),
    name='cos',
    mode='markers',
    marker_color='rgba(255, 182, 193, .9)'
))

x_unidades = np.arange(0, 10)
escalones = [ 1 if x % 2 == 0 else -1 for x in x_unidades ]

fig.add_trace(go.Bar(
    x = x_unidades, y = escalones,
    name = "Escalones"
))

fig.update_layout(title='Seno y Coseno', yaxis_zeroline=False, xaxis_zeroline=False)

# Hace falta: conda install -c plotly python-kaleido
fig.write_image("trigo2.png")

fig.show()