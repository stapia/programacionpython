#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 27 18:39:41 2020

@author: Santiago 
"""

hidrogeno = 1
oxigeno = 16.0001
calcio = 40 

masaTotal = calcio + 2 * (oxigeno + hidrogeno)

print(masaTotal)

porcentajeCalcio = calcio / masaTotal * 100
porcentajeOxigeno = 2 * oxigeno / masaTotal * 100
porcentajeHidrogeno = 2 * hidrogeno / masaTotal * 100

cien = porcentajeCalcio + porcentajeOxigeno + porcentajeHidrogeno

print(porcentajeOxigeno)
