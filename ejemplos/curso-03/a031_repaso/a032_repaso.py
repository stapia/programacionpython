
def transformar(a_list):
    result = [ 2*x for x in a_list ]
    return "resultado", result

if __name__ == "__main__": 
    mi_lista = [ 2, 3, 10 ]
    mi_resultado = transformar(mi_lista)
    print(mi_resultado)

    mi_lista = [ 2, 3, 10, -34 ]
    mi_resultado = transformar(mi_lista)
    print(mi_resultado)

    mi_lista = [  ]
    mi_resultado = transformar(mi_lista)
    print(mi_resultado)

    mi_lista = [ "Hola" ]
    mi_resultado = transformar(mi_lista)
    print(mi_resultado)
