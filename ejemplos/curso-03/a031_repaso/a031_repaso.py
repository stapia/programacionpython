
print("Hola mundo")

vector = [ 2, "hola", 2.34 ]

vector.append(99)

for x in vector:
    print("Elemento: " + str(x))

print(vector)
print(vector[1])

vector2 = [ "Transformado: " + str(x) for x in vector ]

print(vector2)

tuplas = ( 23, "adios", None, 2.4 )

print(tuplas)

tuplas = ( 23, "adios", 2.4, 99 )

print(tuplas)

di = { "uno": 2.4, "dos": "hola", 5: (1, 2) }
print(di)
print(di["uno"])

