
def sumar(x, y):
    return x + y

def resta(x, y):
    return x - y

print(sumar(3,4))
print(resta(y = 10, x = 5))
print(resta(10, 5))

def aplicar(lista1, lista2, fun):
    result = [ fun(x,y) for (x, y) in zip(lista1, lista2) ]
    return result

L1 = [ 32, 43, 66 ]
L2 = [ 22, 13, 66 ]

funciones = ( sumar, resta, sumar )

for f in funciones:
    result = aplicar(L1, L2, f)
    print(result)
