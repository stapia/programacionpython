
def sumar_valores(filename):
    # try with resources con liberación del recurso con close automática
    try: 
        with open(filename, "r") as f:
            # Lee línea a línea, f se comporta como una "lista" de líneas
            for line in f:
                list_num = [ int(str_x) for str_x in line.split() ]
                sum = 0
                for num in list_num:
                    sum += num
                print(f"La suma es {sum}")
    except:
        print(f"Se ha producido un error en '{filename}'")

import glob

filename_list = glob.glob("input/data*.txt")
print(filename_list)

for filename in filename_list:
    print(f"Sumando el archivo: '{filename}'")
    sumar_valores(filename)