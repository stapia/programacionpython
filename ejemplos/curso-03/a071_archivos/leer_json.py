
import json

def leer(filename):
    with open(filename, "r") as data:
        my_data = json.load(data)
        print(my_data)

        print(f"El radio es: {my_data['circle']['radius']}, de tipo {type(my_data['circle']['radius'])}")
        print(f"La etiqueta es: {my_data['circle']['label']}")

        return my_data

origin = leer("data.json")

origin['circle']['radius'] = 99999

import pickle

with open("data.pickle", "wb") as f: 
    pickle.dump(origin, f)

