
import logging
import json

with open("logging.json") as config:
    options = json.load(config)
    # logging.basicConfig(filename='app.log', format='%(name)s - %(levelname)s - %(message)s')
    # Voy a pasarte todos los argumento con "clave" en un dictionary
    logging.basicConfig(**options)

logging.info("Empezando el programa")

logging.warning("Principio del bucle")

for x in [ 1, 4, 7, 10 ]:
    logging.debug(f"Voy por x = {x}")

logging.warning("Fin del bucle")
