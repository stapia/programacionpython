
import tkinter as tk

root = tk.Tk()

label1 = tk.Label(root, text="Hola Mundo", padx=20, pady=20)
label1.pack() 

entry1 = tk.Entry(root)
entry1.pack()

def ponerTexto():
    texto = entry1.get()
    print(texto)
    label1.text.set(texto)

btn1 = tk.Button(root, text="Pulsa", command=ponerTexto)
btn1.pack()

root.mainloop()