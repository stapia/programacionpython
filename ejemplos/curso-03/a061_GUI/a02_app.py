
import tkinter as tk
import tkinter.filedialog as fd

root = tk.Tk()

label1 = tk.Label(root, text="Esta es la imagen:", padx=20, pady=20)
label1.pack() 

canvas = tk.Canvas(root, height=700, width=700)
canvas.pack()

canvas.create_rectangle(5,5,695,695)
canvas.create_line(5,5, 100,200)

def dibujar_oval(event):
    print(event)
    canvas.create_oval(event.x-5, event.y-5, event.x+5, event.y+5)

canvas.bind('<Button-1>', dibujar_oval)

result = fd.askopenfile()
if result != None:
    print(result.name)
    img = tk.PhotoImage(file = result.name)
    canvas.create_image(50,50, anchor=tk.NW, image=img)
else:
    print("Usuario ha cancelado")

canvas.create_line(105,105, 200,200)

root.mainloop()
