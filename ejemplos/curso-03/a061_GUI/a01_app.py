
import tkinter as tk

def decir_hola(): 
    print("Hola")

print("Empezando...")

root = tk.Tk()

frm1 = tk.Frame(root, width= 300, height=800)
frm1.pack(side = tk.LEFT)

frm2 = tk.Frame(root, width= 300, height=400)
frm2.pack(side = tk.RIGHT)

label1 = tk.Label(frm1, text="Hola Mundo", padx=20, pady=20)
label1.pack() # o grid

label2 = tk.Label(frm2, text="Soy una etiqueta", padx=20, pady=20)
label2.pack()

label3 = tk.Label(frm2, text="Soy un texto", padx=10, pady=10)
label3.pack()

btn1 = tk.Button(frm1, text="Pulsa", command=decir_hola)
btn1.pack()

print("Bucle principal")

root.mainloop()

decir_hola()

print("Terminando...")
