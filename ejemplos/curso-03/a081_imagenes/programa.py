
import PIL.Image
import PIL.ImageTk
import PIL.ImageFilter

import tkinter as tk
import tkinter.filedialog as fd

import cv2

import numpy as np

print("Starting...")

root = tk.Tk()

frame0 = tk.Frame(root)
canvas = tk.Canvas(frame0, width=1024, height=1024)
canvas.create_rectangle(2,2,1022,1022)

# Esta es la imagen que se va a dibujar en el canvas
img_tk = None 

def abrirImagen():
    path = fd.askopenfilename()
    if path != None:
        print("Abriendo archivo: '" + path + "'")
        img = PIL.Image.open(path)
        global img_tk
        img_tk = PIL.ImageTk.PhotoImage(image=img)
        canvas.create_image(0,0, image = img_tk, anchor=tk.NW)
    else: 
        print("Operacion cancelada")

filterDict = {
    "BLUR" : PIL.ImageFilter.BLUR,
    "CONTOUR" : PIL.ImageFilter.CONTOUR,
    "DETAIL" : PIL.ImageFilter.DETAIL,
    "EDGE" : PIL.ImageFilter.EDGE_ENHANCE
}

def filtroImagen(filterName):
    if not filterName in filterDict:
        return 

    path = fd.askopenfilename()
    if path != None:
        img = PIL.Image.open(path)
        imgFiltered = img.filter(filterDict[filterName])
        global img_tk
        img_tk = PIL.ImageTk.PhotoImage(image=imgFiltered)
        canvas.create_image(0,0, image = img_tk, anchor=tk.NW)

def edgeDetection():
    path = fd.askopenfilename()
    if path != None:
        img_cv = cv2.imread(path)
        edges = cv2.Canny(img_cv,100,200)
        img = PIL.Image.fromarray(cv2.cvtColor(edges, cv2.COLOR_BGR2RGB))
        global img_tk
        img_tk = PIL.ImageTk.PhotoImage(image=img)
        canvas.create_image(0,0, image = img_tk, anchor=tk.NW)

def cornerDetection():
    path = fd.askopenfilename()
    if path != None:
        img_cv = cv2.imread(path)
        gray = cv2.cvtColor(img_cv,cv2.COLOR_BGR2GRAY)
        corners = cv2.goodFeaturesToTrack(gray,25,0.01,10)
        corners = np.int0(corners)
        for i in corners:
            x,y = i.ravel()
            cv2.circle(img_cv,(x,y),3,255,-1)

        img = PIL.Image.fromarray(cv2.cvtColor(img_cv, cv2.COLOR_BGR2RGB))
        global img_tk
        img_tk = PIL.ImageTk.PhotoImage(image=img)
        canvas.create_image(0,0, image = img_tk, anchor=tk.NW)

frame1 = tk.Frame(root)
label1 = tk.Label(frame1, text="Abrir Imagen")
btn1 = tk.Button(frame1, text="[...]", command=abrirImagen)

frame1.pack()
label1.pack(side=tk.LEFT)
btn1.pack(side=tk.LEFT)

frame2 = tk.Frame(root)
label2 = tk.Label(frame2, text="Filtrar Imagen")
text2 = tk.Entry(frame2)
btn2 = tk.Button(frame2, text="[...]", command = lambda : filtroImagen(text2.get()) )

frame2.pack()
label2.pack(side=tk.LEFT)
text2.pack(side=tk.LEFT)
btn2.pack(side=tk.LEFT)

frame3 = tk.Frame(root)
label3 = tk.Label(frame3, text="OpenCV Imagen")
btn3 = tk.Button(frame3, text="[...]", command = cornerDetection )

frame3.pack()
label3.pack(side=tk.LEFT)
btn3.pack(side=tk.LEFT)

frame0.pack()
canvas.pack()

root.mainloop()
