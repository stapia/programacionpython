
def es_primo(n):
    for i in range(2, n):
        if n % i == 0: return False
    return True

if __name__ == "__main__":
    print("Estoy ejecutando como main")
    print("Y aquí meto las pruebas para saber si está bien")

    print(f"2 es primo es {es_primo(2)}")
    print(f"6 es primo es {es_primo(6)}")
    print(f"13 es primo es {es_primo(13)}")
